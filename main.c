#include <stdio.h>

const char *get_hello_world()
{
  return "Hello World\n";
}

int main(int argc, char *argv[])
{
  printf(get_hello_world());
  return 0;
}